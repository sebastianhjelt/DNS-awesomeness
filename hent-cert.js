var request = require('request')

// 1. Få udstedt certifikat fra LetsEncrypt
// Det skal foregå med DNS-01 challenge, du giver vores domæne "bytesandbrains.dk"
// og får en TXT DNS Record

console.log(process.env.CERTBOT_VALIDATION)

// 2. Sæt TXT DNS Record ind i iwantmyname.com, dette kræver vores brugernavn/kodeord

request.get('https://iwantmyname.com/basicauth/ddns?hostname=_acme-challenge.bytesandbrains.dk&type=txt&value='+process.env.CERTBOT_VALIDATION, {
  'auth': {
    'user': 'zacharias@laosdirg.com',
    'pass': 'madness?thisislaosdirg',
  }
}, function(error, response, body) {
  console.log(error, response, body)
}); // Henter certifikat gennem certbot og ?lagrer?

// 3. Nu kan LetsEncrypt verificere at domænet er vores, og du får et certifikat
// et certikat er 2 filer, "certificate" og "key"
console.log('verificer med LetsEncrypt')

setTimeout(function(){
}, 30*1000) // Kører i 30 sekunder inden upload-til-gitlab.js
